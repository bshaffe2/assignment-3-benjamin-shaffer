﻿using System;
using System.Drawing;
using ShapeAnimator.Model;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     A derived class of ShapeSprite representing a square.
    /// </summary>
    public class SquareSprite : ShapeSprite
    {
        private readonly Color theColor;

        /// <summary>
        ///     Prevents a default instance of the <see cref="SquareSprite" /> class from being created.
        /// </summary>
        protected SquareSprite()
        {
            this.TheShape = null;
            this.SetWidth(20);
            this.SetHeight(20);
            int redValue = Randomizer.Random.Next(256);
            int greenValue = Randomizer.Random.Next(256);
            int blueValue = Randomizer.Random.Next(256);
            this.theColor = Color.FromArgb(redValue, greenValue, blueValue);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SquareSprite" /> class.
        ///     Precondition: shape != null
        /// </summary>
        /// <param name="shape">The shape.</param>
        public SquareSprite(Shape shape) : this()
        {
            if (shape == null)
            {
                throw new ArgumentNullException("shape");
            }

            this.TheShape = shape;
        }

        /// <summary>
        ///     Paints the specified g.
        /// </summary>
        /// <param name="g">The g.</param>
        /// <exception cref="System.ArgumentNullException">g</exception>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if (this.TheShape == null)
            {
                return;
            }
            var greenBrush = new SolidBrush(this.theColor);
            g.FillRectangle(greenBrush, this.TheShape.X, this.TheShape.Y, this.GetWidth(), this.GetHeight());
        }
    }
}