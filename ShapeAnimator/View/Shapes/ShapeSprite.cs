﻿using System.Drawing;
using ShapeAnimator.Model;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     An abstract class representing a Sprite object.
    /// </summary>
    public abstract class ShapeSprite
    {
        /// <summary>
        ///     The shape
        /// </summary>
        protected Shape TheShape;

        private int height;
        private int width;

        /// <summary>
        ///     Paints the specified g.
        /// </summary>
        /// <param name="g">The g.</param>
        public abstract void Paint(Graphics g);

        /// <summary>
        ///     Gets the width.
        /// </summary>
        /// <returns></returns>
        public int GetWidth()
        {
            return this.width;
        }

        /// <summary>
        ///     Gets the height.
        /// </summary>
        /// <returns></returns>
        public int GetHeight()
        {
            return this.height;
        }

        /// <summary>
        ///     Sets the width.
        /// </summary>
        /// <param name="theWidth">The width.</param>
        protected void SetWidth(int theWidth)
        {
            this.width = theWidth;
        }

        /// <summary>
        ///     Sets the height.
        /// </summary>
        /// <param name="theHeight">The height.</param>
        protected void SetHeight(int theHeight)
        {
            this.height = theHeight;
        }
    }
}