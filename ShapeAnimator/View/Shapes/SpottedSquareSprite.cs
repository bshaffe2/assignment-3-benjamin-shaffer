﻿using System;
using System.Drawing;
using ShapeAnimator.Model;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     A derived class of ShapeSprite representing a spotted square.
    /// </summary>
    public class SpottedSquareSprite : SquareSprite
    {
        private const int SpotWidth = 10;
        private const int SpotHeight = 15;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SquareSprite" /> class.
        ///     Precondition: shape != null
        /// </summary>
        /// <param name="shape">The shape.</param>
        public SpottedSquareSprite(Shape shape)
        {
            if (shape == null)
            {
                throw new ArgumentNullException("shape");
            }

            this.TheShape = shape;
        }

        /// <summary>
        ///     Paints the specified g.
        /// </summary>
        /// <param name="g">The g.</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);
            var redBrush = new SolidBrush(Color.DarkRed);
            g.FillEllipse(redBrush, this.TheShape.X + 5, this.TheShape.Y + 2, SpotWidth, SpotHeight);
        }
    }
}