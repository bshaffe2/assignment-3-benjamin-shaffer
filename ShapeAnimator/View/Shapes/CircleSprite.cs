﻿using System;
using System.Drawing;
using ShapeAnimator.Model;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public class CircleSprite : ShapeSprite
    {
        private readonly Color theColor;

        /// <summary>
        ///     Prevents a default instance of the <see cref="CircleSprite" /> class from being created.
        /// </summary>
        private CircleSprite()
        {
            this.TheShape = null;
            this.SetWidth(50);
            this.SetHeight(50);
            int redValue = Randomizer.Random.Next(256);
            int greenValue = Randomizer.Random.Next(256);
            int blueValue = Randomizer.Random.Next(256);
            this.theColor = Color.FromArgb(redValue, greenValue, blueValue);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CircleSprite" /> class.
        ///     Precondition: shape != null
        /// </summary>
        /// <param name="shape">The shape.</param>
        public CircleSprite(Shape shape) : this()
        {
            if (shape == null)
            {
                throw new ArgumentNullException("shape");
            }

            this.TheShape = shape;
        }

        /// <summary>
        ///     Draws a shape
        ///     Preconditon: g != null
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if (this.TheShape == null)
            {
                return;
            }

            var yellowBrush = new SolidBrush(this.theColor);
            g.FillEllipse(yellowBrush, this.TheShape.X, this.TheShape.Y, this.GetWidth(), this.GetHeight());
        }
    }
}