﻿using System;

namespace ShapeAnimator.Model
{
    internal static class Randomizer
    {
        public static readonly Random Random;

        static Randomizer()
        {
            Random = new Random();
        }
    }
}