﻿using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model
{
    internal class Square : Shape
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Square" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public Square(int x, int y) : base(x, y)
        {
            this.X = x;
            this.Y = y;
            this.SetSprite(new SquareSprite(this));
            this.CalculateDirection();
        }

        public override void Move()
        {
            switch (this.Direction)
            {
                case -1:
                    this.Y = this.Y - this.GetSpeed();
                    break;

                case 1:
                    this.Y += this.GetSpeed();
                    break;
            }
        }
    }
}