using System;
using System.Drawing;
using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model
{
    /// <summary>
    ///     Holds information about a shape
    /// </summary>
    public class Shape
    {
        #region Instance variables

        private readonly int speed;
        private Point location;
        private ShapeSprite sprite;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the x location of the shape.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        public int X
        {
            get { return this.location.X; }
            set { this.location.X = value; }
        }

        /// <summary>
        ///     Gets or sets the y location of the shape.
        /// </summary>
        /// <value>
        ///     The y.
        /// </value>
        public int Y
        {
            get { return this.location.Y; }
            set { this.location.Y = value; }
        }

        /// <summary>
        ///     Gets or sets the direction.
        /// </summary>
        /// <value>
        ///     The direction.
        /// </value>
        public int Direction { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Constructs a shape at specified location
        ///     Precondition: location != null
        ///     Postcondition: X == location.X; Y == location.Y
        /// </summary>
        /// <param name="location">Location to create shape</param>
        public Shape(Point location)
        {
            if (location == null)
            {
                throw new ArgumentNullException("location");
            }

            this.location = location;
        }

        /// <summary>
        ///     Constructs a shape specified x,y location
        ///     Precondition: None
        ///     Postcondition: X == x; Y == y
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public Shape(int x, int y) : this(new Point(x, y))
        {
            this.location = new Point(x, y);
            this.speed = Randomizer.Random.Next(1, 5);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Randomly moves a shape in the x, y direction
        ///     Precondition: None
        ///     Postcondition: X == X@prev + amount of movement in X direction; Y == Y@prev + amount of movement in Y direction
        /// </summary>
        public virtual void Move()
        {
            this.X += Randomizer.Random.Next(-5, 6);
            this.Y += Randomizer.Random.Next(-5, 6);
        }

        /// <summary>
        ///     Draws a shape
        ///     Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            this.sprite.Paint(g);
        }

        /// <summary>
        ///     Calculates the direction.
        /// </summary>
        public void CalculateDirection()
        {
            int directionIndex = Randomizer.Random.Next(0, 2);
            switch (directionIndex)
            {
                case 0:
                    this.Direction = -1;
                    break;
                case 1:
                    this.Direction = 1;
                    break;
            }
        }

        /// <summary>
        ///     Sets the sprite.
        /// </summary>
        /// <param name="theSprite">The sprite.</param>
        protected void SetSprite(ShapeSprite theSprite)
        {
            this.sprite = theSprite;
        }

        /// <summary>
        ///     Gets the speed.
        /// </summary>
        /// <returns>
        ///     The speed of the shape, a number 1 through 5.
        /// </returns>
        protected int GetSpeed()
        {
            return this.speed;
        }

        /// <summary>
        ///     Changes the shapes direction from -1 to 1 or vice versa.
        /// </summary>
        public void ChangeShapesDirection()
        {
            if (this.Direction == 1)
            {
                this.Direction = -1;
            }
            else
            {
                this.Direction = 1;
            }
        }

        /// <summary>
        ///     Gets the width of the sprite.
        /// </summary>
        /// <returns></returns>
        public int GetSpriteWidth()
        {
            return this.sprite.GetWidth();
        }

        /// <summary>
        ///     Gets the height of the sprite.
        /// </summary>
        /// <returns></returns>
        public int GetSpriteHeight()
        {
            return this.sprite.GetHeight();
        }

        #endregion
    }
}