﻿using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model
{
    internal class SpottedSquare : Square
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SpottedSquare" /> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public SpottedSquare(int x, int y) : base(x, y)
        {
            this.SetSprite(new SpottedSquareSprite(this));
            this.CalculateDirection();
        }

        public override void Move()
        {
            switch (this.Direction)
            {
                case -1:
                    this.Y = this.Y - this.GetSpeed();
                    break;

                case 1:
                    this.Y += this.GetSpeed();
                    break;
            }
        }
    }
}