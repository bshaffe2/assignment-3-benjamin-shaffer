﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ShapeAnimator.Properties;

namespace ShapeAnimator.Model
{
    /// <summary>
    ///     Manages the collection of shapes on the canvas.
    /// </summary>
    public class ShapeManager
    {
        #region Instance variables

        private readonly PictureBox canvas;
        private readonly List<Shape> shapes;
        private Shape shape;

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeManager" /> class from being created.
        /// </summary>
        private ShapeManager()
        {
            this.shape = null;
            this.shapes = new List<Shape>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeManager" /> class.
        ///     Precondition: pictureBox != null
        /// </summary>
        /// <param name="pictureBox">The picture box that will be drawing on</param>
        public ShapeManager(PictureBox pictureBox) : this()
        {
            if (pictureBox == null)
            {
                throw new ArgumentNullException("pictureBox", Resources.PictureBoxNullMessage);
            }

            this.canvas = pictureBox;
        }

        #endregion

        /// <summary>
        ///     Places the shape on the canvas.
        ///     Precondition: None
        /// </summary>
        public void PlaceShapesOnCanvas(int numberOfShapes)
        {
            this.shapes.Clear();

            for (int i = 0; i < numberOfShapes; i++)
            {
                const int x = 0;
                const int y = 0;
                this.shape = ShapeFactory.GetRandomShape(x, y);
                this.shape.X = Randomizer.Random.Next(this.canvas.Width - this.shape.GetSpriteWidth());
                this.shape.Y = Randomizer.Random.Next(this.canvas.Height - this.shape.GetSpriteHeight());
                this.shapes.Add(this.shape);
            }
        }

        /// <summary>
        ///     Moves the shape around and the calls the Shape::Paint method to draw the shape.
        ///     Precondition: g != null
        /// </summary>
        /// <param name="g">The graphics object to draw on</param>
        public void Update(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if (this.shape != null)
            {
                foreach (Shape aShape in this.shapes)
                {
                    aShape.Paint(g);
                    this.moveShapesAndBounceOffWalls(aShape);
                }
            }
        }

        private void moveShapesAndBounceOffWalls(Shape aShape)
        {
            if (aShape.X > (this.canvas.Width - aShape.GetSpriteWidth()) || aShape.X < 0)
            {
                aShape.ChangeShapesDirection();
                aShape.Move();
            }
            else
            {
                aShape.Move();
            }

            if (aShape.Y > (this.canvas.Height - aShape.GetSpriteHeight()) || aShape.Y < 0)
            {
                aShape.ChangeShapesDirection();
                aShape.Move();
            }
            else
            {
                aShape.Move();
            }
        }
    }
}