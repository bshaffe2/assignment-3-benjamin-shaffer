﻿namespace ShapeAnimator.Model
{
    internal class ShapeFactory
    {
        private static Shape randomShape;

        public ShapeFactory()
        {
            randomShape = null;
        }

        internal static Shape GetRandomShape(int x, int y)
        {
            ShapeType[] values = {ShapeType.Circle, ShapeType.Square, ShapeType.SpottedSquare};
            int randomType = Randomizer.Random.Next(values.Length);
            ShapeType randomShapeType = values[randomType];

            switch (randomShapeType)
            {
                case ShapeType.Circle:
                    randomShape = new Circle(x, y);
                    break;

                case ShapeType.Square:
                    randomShape = new Square(x, y);
                    break;

                case ShapeType.SpottedSquare:
                    randomShape = new SpottedSquare(x, y);
                    break;
            }
            return randomShape;
        }

        private enum ShapeType
        {
            Circle,
            Square,
            SpottedSquare
        }
    }
}