﻿using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model
{
    internal class Circle : Shape
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Circle" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public Circle(int x, int y) : base(x, y)
        {
            this.X = x;
            this.Y = y;
            this.SetSprite(new CircleSprite(this));
            this.CalculateDirection();
        }

        /// <summary>
        ///     Randomly moves a shape in the x, y direction
        ///     Precondition: None
        ///     Postcondition: X == X@prev + amount of movement in X direction; Y == Y@prev + amount of movement in Y direction
        /// </summary>
        public override void Move()
        {
            switch (this.Direction)
            {
                case -1:
                    this.X = this.X - this.GetSpeed();
                    break;

                case 1:
                    this.X += this.GetSpeed();
                    break;
            }
        }
    }
}